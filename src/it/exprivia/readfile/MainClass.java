package it.exprivia.readfile;

import java.nio.file.Files;
import java.nio.file.Paths;

import it.exprivia.readfile.tar.TAR;
import it.exprivia.readfile.zip.ZIP;

public class MainClass
{
	public static void main(String[] args)
	{
		System.out.println();
		
		try
		{
			Files.createDirectories(Paths.get("./output-tar"));
			Files.createDirectories(Paths.get("./output-zip"));

			TAR.readFileWithoutUnpacking();
			ZIP.readFileWithoutUnpacking();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		System.out.println();
	}
}