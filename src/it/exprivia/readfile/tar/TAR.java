package it.exprivia.readfile.tar;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

import java.nio.charset.StandardCharsets;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

import it.exprivia.readfile.GenericFormat;

/*
 * COPERNICUS 
 * */
public class TAR extends GenericFormat
{
	public static void readFileWithoutUnpacking() throws FileNotFoundException, IOException{

		File file = new File("./input-tar/s3a-product.tar");

		InputStream inputStream = new FileInputStream (file);

		TarArchiveInputStream tarArchiveInputStream = new TarArchiveInputStream ( inputStream );

		TarArchiveEntry tarArchiveEntry = null;

	    try
	    {
	    	while (( tarArchiveEntry = tarArchiveInputStream.getNextTarEntry()) != null){

	    		if ( tarArchiveEntry.isFile() ){
	    			String fileName = new File(tarArchiveEntry.getName()).getName();

		            if ( fileName.equalsIgnoreCase("manifest.xml") ){
		            	extractFile(tarArchiveInputStream, "manifest.xml");
		            }

		            if ( fileName.equalsIgnoreCase("geolocation.geo") ){
		            	extractFile(tarArchiveInputStream, "geolocation.geo");
		            }

		            if ( fileName.startsWith("browse") ){
		            	extractImageFile(tarArchiveInputStream, fileName);
		            }

		            if ( fileName.equalsIgnoreCase("data") ){
		            	extractFile(tarArchiveInputStream, "data");
		            }
                }
	        }
	    }
	    finally{
	    	try{
	    		tarArchiveInputStream.close();
	        }
	        catch (Exception e){
	            e.printStackTrace();
	        }
	    }
	}

	private static void extractFile(TarArchiveInputStream tarArchiveInputStream, String fileName) throws IOException{

		BufferedReader bufferedReader = new BufferedReader ( new InputStreamReader ( tarArchiveInputStream ) );
        StringBuilder  stringBuilder  = new StringBuilder(4096);
        String line = null;

        while ((line = bufferedReader.readLine()) != null){
        	stringBuilder.append(line);
        	stringBuilder.append('\n');
        }

    	System.out.println (stringBuilder.toString());

    	byte[] stringBuilderBytes = stringBuilder.toString().getBytes ( ( StandardCharsets.UTF_8 ) );

    	Path path = Paths.get("./output-tar/" + fileName);

    	Files.write ( path, stringBuilderBytes );
	}

	private static void extractImageFile(TarArchiveInputStream tarArchiveInputStream, String fileName) throws IOException{

    	byte[] image = getImage(tarArchiveInputStream);

        Path path = Paths.get("./output-tar/" + fileName);

        Files.write ( path, image );
	}
}