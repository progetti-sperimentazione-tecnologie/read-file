package it.exprivia.readfile.zip;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;

import java.nio.charset.StandardCharsets;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;

import java.util.Enumeration;

import org.apache.commons.compress.archivers.zip.ZipFile;

import it.exprivia.readfile.GenericFormat;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;

/*
 * MTG
 * */
public class ZIP extends GenericFormat{

	public static void readFileWithoutUnpacking() throws FileNotFoundException, IOException
	{
		ZipFile zipFile = new ZipFile("./input-zip/mtg-product.zip");

	    BufferedReader  bufferedReader = null;

	    try
	    {
	    	Enumeration<ZipArchiveEntry> entries = zipFile.getEntries();

	    	while ( entries.hasMoreElements() )
	    	{
	    		ZipArchiveEntry zipArchiveEntry    = (ZipArchiveEntry) entries.nextElement();
	    		InputStream     entryAsInputStream = zipFile.getInputStream(zipArchiveEntry);

	    		Boolean isFile = !zipArchiveEntry.isDirectory(); 

	    		if (isFile){
	    			String fileName = new File(zipArchiveEntry.getName()).getName();

	    			System.out.println("nome file " + fileName);

	    			if ( fileName.startsWith("manifest") ){
		            	extractFile(entryAsInputStream, fileName);
		            }

		            if ( fileName.startsWith("geolocation") ){
		            	extractFile(entryAsInputStream, fileName);
		            }

		            if ( fileName.startsWith("browse") ){
		            	extractImageFile(entryAsInputStream, fileName);
		            }

		            if ( fileName.equals("data") ){
		            	extractFile(entryAsInputStream, "data");
		            }
	    		}
	        }
	    }
	    finally{
	        if (bufferedReader != null){
	            try{
	                bufferedReader.close();
	                zipFile.close();
	            }
	            catch (Exception e){
	                e.printStackTrace();
	            }
	        }
	    }
	}

	private static void extractFile(InputStream entryAsInputStream, String fileName) throws IOException{

        BufferedReader reader = new BufferedReader( new InputStreamReader(entryAsInputStream) );

	    StringBuilder stringBuilder = new StringBuilder(4096);
	    String line;

        while ( (line = reader.readLine()) != null ){
        	stringBuilder.append(line);
        	stringBuilder.append('\n');
        }

    	byte[] stringBuilderBytes = stringBuilder.toString().getBytes ( ( StandardCharsets.UTF_8 ) );

    	Path path = Paths.get("./output-zip/" + fileName);

    	Files.write ( path, stringBuilderBytes );
	}
	
	private static void extractImageFile(InputStream entryAsInputStream, String fileName) throws IOException{

    	byte[] image = getImage(entryAsInputStream);

        Path path = Paths.get("./output-zip/" + fileName);

        Files.write ( path, image );
	}
}