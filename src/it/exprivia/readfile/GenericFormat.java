package it.exprivia.readfile;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class GenericFormat{
	
	public static byte[] getImage(InputStream inputStream) throws IOException{
		BufferedImage image = ImageIO.read(inputStream);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		ImageIO.write(image, "jpg", baos);

		return baos.toByteArray();
	}
}